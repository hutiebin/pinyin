(defproject org.clojars.dumuji/pinyin "1.2.0"
  :description "汉字转拼音首字母的Clojure(Script)库"
  :url "https://gitee.com/hutiebin/pinyin"
  :scm {:name "git" :url "https://gitee.com/hutiebin/pinyin"}
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.11.1"]
                 [org.clojure/clojurescript "1.11.132"]]
  :plugins [[lein-cljsbuild "1.1.8"]]
  :cljsbuild {:builds {:app {:source-paths ["src"]
                             :compiler {:output-to "target/cljsbuild/public/js/pinyin.js"
                                        :output-dir "target/cljsbuild/public/js/out"
                                        :asset-path "js/out"
                                        
                                        :optimizations :advanced
                                        :source-map "target/cljsbuild/public/js/source_map"
                                        
                                        :pretty-print true}}}}
  :clean-targets ^{:protect false} [:target-path
                                    [:cljsbuild :builds :app :compiler :output-dir]
                                    [:cljsbuild :builds :app :compiler :output-to]]
  :target-path "target/%s"
  :resource-paths ["target" "resources"]
  :repl-options {:init-ns pinyin.core})
