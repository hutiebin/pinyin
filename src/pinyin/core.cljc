(ns pinyin.core
  (:require [clojure.string :as str]
            #?@(:cljs [[pinyin.dict :refer [dict]]]
                :clj [[clojure.java.io :as io]
                      [clojure.edn :as edn]])))

;;java函数有64k的大小限制，因此不能像cljs那样直接定义dict的字面量
#?(:clj (def dict (-> "dict.edn"
                      io/resource
                      slurp
                      edn/read-string)))

(defn- char-code
  "字符编码"
  [ch]
  #?(:clj (int ch)
     ;;注意，js没有字符类型，只有字符串
     :cljs (.charCodeAt ch 0)))

(defn- char-py
  [ch]
  (get dict (char-code ch)))

(defn py
  "将字符串中的汉字转换为拼音首字母，忽略非汉字
  返回一个序列，序列的元素是由对应汉字的多种读音的首字母组成的字符串
  当full?参数为true时保留非汉字字符并转为小写，否则丢弃非汉字字符"
  ([string] (py string false))
  ([string full?]
   (let [tr (cond-> char-py full? (some-fn str))]
     (keep tr string))))

(defn pinyin
  "将字符串中的汉字转换为拼音首字母，忽略非汉字
  与py函数不同，本函数返回一个字符串，多音字的多个首字母用小括号包围
  当full?参数为true时保留非汉字字符并转为小写，否则丢弃非汉字字符"
  ([string] (pinyin string false))
  ([string full?]
   (->> (py string full?)
        (map #(if (> (count %) 1) (str "(" % ")") %))
        (apply str))))

#_(defn- match?
    "判断两个拼音是否匹配
  pinyin是完整版(字符串序列)，py是简化版(字符序列)
  successive?表示必须连续匹配
  head-fix?表示固定从头部开始匹配"
    [pinyin py successive? head-fix?]
    (if (empty? py)
      true
      (if (empty? pinyin)
        false
        (let [[s1 & pinyin] pinyin
              first-matched? (str/index-of s1 (first py))
              py (cond-> py first-matched? rest)]
          (if (or successive? head-fix?)
            (if head-fix?
              (if first-matched?
                (recur pinyin py successive? successive?)
                false)
              ;;以下错误，第一个匹配阻止了后续连续项的匹配
              (recur pinyin py true first-matched?))
            (recur pinyin py false false))))))

(defn- match?
  "判断两个拼音是否匹配
  pinyin是完整版(字符串序列)，py是简化版(字符序列)
  successive?表示必须连续匹配
  head-fix?表示固定从头部开始匹配"
  [pinyin py successive? head-fix?]
  (if (empty? py)
    true
    (if (empty? pinyin)
      false
      (let [[s1 & pinyin] pinyin]
        ;;有两种情况匹配成功
        ;;1.第一个字符匹配，并且后续的pinyin也匹配后续的py
        ;;2.在不要求头部匹配的情况下，后续的pinyin匹配整个py
        (or (and (str/index-of s1 (first py))
                 (match? pinyin (rest py) successive? successive?))
            (and (not head-fix?)
                 (match? pinyin py successive? false)))))))

(defn py?
  "判断汉字字符串string是否与拼音首字母字符串py相容
  successive?表示必须连续匹配"
  ([string pinyin] (py? string pinyin false))
  ([string pinyin successive?]
   (match? (py string successive?)
           (seq pinyin)
           successive?
           false)))

(defn- py-seq
  "切割字符串，考虑小括号
  ```Clojure
  (py-seq \"a(bc)(de)f\")
  ;;=> (\"a\" \"bc\" \"de\" \"f\")
  ```
  "
  [pinyin]
  (lazy-seq
   (when-not (str/blank? pinyin)
     (if (str/starts-with? pinyin "(")
       (let [i (str/index-of pinyin ")")]
         (cons (subs pinyin 1 i)
               (py-seq (subs pinyin (inc i)))))
       (cons (subs pinyin 0 1)
             (py-seq (subs pinyin 1)))))))

(defn pinyin?
  "比较两个字符串是否能部分匹配
  pinyin是某汉字字符串经函数pinyin变换得到的字符串
  py是简化版拼音
  successive?表示必须连续匹配"
  ([pinyin py] (pinyin? pinyin py false))
  ([pinyin py successive?]
   (match? (py-seq pinyin)
           (seq py)
           successive?
           false)))
