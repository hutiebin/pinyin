# pinyin

将汉字转换为拼音首字母的Clojure(Script)库，可进行汉字字符串与拼音首字母的相容判断

## Usage

### 将汉字字符串转换为拼音首字母

```clj
(use 'pinyin.core)

(py "曾经的岁月")
;;=> ("cz" "j" "d" "s" "y")
```

本函数将字符串中的汉字转换为拼音首字母（小写），忽略非汉字。返回一个字符串的序列，每个字符串都是对应汉字的多种读音的首字母组成的字符串。如果对应汉字只有一个读音，或多个读音的首字母相同，则该字符串只包含一个字符。

如果希望原样保留字符串中的非汉字字符，可以传入第二个参数true，如

```clj
(py "A曾经的岁月" true)
;;=> ("A" "cz" "j" "d" "s" "y")
```

如果希望返回值是一个包含所有首字母的字符串，可以使用：

```clj
(pinyin "曾经的岁月")
;;=> "(cz)jdsy"
```

多音字的首字母用小括号包围，以与其他字母分隔。

同样可以传入第二个参数true，以保留非汉字字符。

### 比较汉字字符串与给定的拼音首字母字符串是否匹配

可以检测汉字字符串是否与拼音首字母字符串匹配，这里匹配的含义是汉字字符串的拼音首字母顺次（可以不连续）包含了给定的拼音首字母，如

```clj
(py? "公司人力资源部" "rzb")
;;=> true
```

对多音字，所有拼音首字母均纳入相容判断，而不考虑当前字符串中的实际读音。(判断实际读音太复杂了:)

如果要求连续匹配，则可传递第三个参数，如

```clj
(py? "公司人力资源部" "rzb" true)
;;=> nil
(py? "公司人力资源部" "rlzyb" true)
;;=> true
```


### 比较两个拼音是否匹配

如果你将汉字字符串的拼音表示（`pinyin`函数的返回值）缓存起来了（例如保存在数据库的一个单独字段），要比较汉字串是否与拼音匹配，可以直接比较这个缓存起来的拼音是否与给定拼音匹配，可以使用`pinyin?`函数，如下所示：

```clj
(pinyin? "(cz)jdsy" "cj")
;;=> true
```

如果要求连续匹配，也可像py?函数一样传入第三个参数true。

## License

Copyright © 2024 FIXME

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

This Source Code may also be made available under the following Secondary
Licenses when the conditions for such availability set forth in the Eclipse
Public License, v. 2.0 are satisfied: GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or (at your
option) any later version, with the GNU Classpath Exception which is available
at https://www.gnu.org/software/classpath/license.html.
